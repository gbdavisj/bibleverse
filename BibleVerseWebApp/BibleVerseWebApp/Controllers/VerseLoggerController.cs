﻿/* CST-247 Enterprise Application Programming III
 * Benchmark
 * VerseLoggerController, Version 1
 * Gary Davis
 * 09/08/2019
 * This controller is used to save the users Bible verse.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BibleVerseWebApp.Models;
using BibleVerseWebApp.Interfaces;
using BibleVerseWebApp.Services;

namespace BibleVerseWebApp.Controllers
{
    public class VerseLoggerController : Controller
    {
        //Using DI to get the data from the database
        private readonly IMyBibleRepository verses;

        public VerseLoggerController() : this(new BibleRepository())
        { }

        public VerseLoggerController(IMyBibleRepository repository)
        {
            verses = repository;
        }


        // GET: VerseLogger
        public ActionResult Index()
        {
            return View();
        }

        //Save clicked and data is saved to the database
        [HttpPost]
        public ActionResult Index([Bind(Exclude = "Id")]BIBLE_ENTRY verseEntry)
        {
            verses.GetBibleEntry().BIBLE_ENTRY.Add(verseEntry);
            verses.GetBibleEntry().SaveChanges();
            return RedirectToAction("Index", "SavedVerse");
        }
    }
}