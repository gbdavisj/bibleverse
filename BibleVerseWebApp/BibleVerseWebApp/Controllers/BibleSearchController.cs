﻿/* CST-247 Enterprise Application Programming III
 * Benchmark
 * BibleSearchController, Version 1
 * Gary Davis
 * 09/08/2019
 * This controller is used to find a verse that the user searched for.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BibleVerseWebApp.Models;
using BibleVerseWebApp.Interfaces;
using BibleVerseWebApp.Services;

namespace BibleVerseWebApp.Controllers
{
    public class BibleSearchController : Controller
    {

        //Using DI to get the data from the database
        private readonly IMyBibleRepository verses;

        public BibleSearchController() : this(new BibleRepository())
        { }

        public BibleSearchController(IMyBibleRepository repository)
        {
            verses = repository;
        }

        // GET: BibleSearch
        public ActionResult Index()
        {
            return View();
        }

        //Submit clicked and using LINQ to query the database.
        [HttpPost]
        public ActionResult Index(BIBLE_VERSES verseEntry)
        {
            ViewData["verseTxt"] = "";

            foreach (var i in this.verses.GetBibleVerses())
            {
                if (i.testament == verseEntry.testament && i.book == verseEntry.book && i.chapter == verseEntry.chapter && i.verse == verseEntry.verse)
                {
                    ViewData["verseTxt"] = i.verse_text;
                }
            }
            if (ViewData["verseTxt"].ToString() == "")
            {
                ViewData["verseTxt"] = "Verse not found!";
            }
            return View();
        }
    }
}