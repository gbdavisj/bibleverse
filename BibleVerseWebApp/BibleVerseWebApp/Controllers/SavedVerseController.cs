﻿/* CST-247 Enterprise Application Programming III
 * Benchmark
 * SavedVerseController, Version 1
 * Gary Davis
 * 09/08/2019
 * This controller is used to get a list of the users saved verses.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BibleVerseWebApp.Interfaces;
using BibleVerseWebApp.Models;
using BibleVerseWebApp.Services;

namespace BibleVerseWebApp.Controllers
{
    public class SavedVerseController : Controller
    {
        //Using DI to get the data from the database
        private readonly IMyBibleRepository verses;

        public SavedVerseController() : this(new BibleRepository())
        { }

        public SavedVerseController(IMyBibleRepository repository)
        {
            verses = repository;
        }

        // GET: SavedVerse
        public ActionResult Index()
        {
            return View(verses.GetBibleEntry().BIBLE_ENTRY.ToList());
        }
    }
}