﻿/* CST-247 Enterprise Application Programming III
 * Benchmark
 * BibleRepository, Version 1
 * Gary Davis
 * 09/08/2019
 * This service is needed to get the database information
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BibleVerseWebApp.Models;
using BibleVerseWebApp.Interfaces;

namespace BibleVerseWebApp.Services
{
    public class BibleRepository : IMyBibleRepository
    {
        BibleVerseDBEntities bbv = new BibleVerseDBEntities();
        public BibleVerseDBEntities GetBibleEntry()
        {
            
            return this.bbv;
        }

        public List<BIBLE_VERSES> GetBibleVerses()
        {
            return bbv.BIBLE_VERSES.ToList();
        }

    }
}