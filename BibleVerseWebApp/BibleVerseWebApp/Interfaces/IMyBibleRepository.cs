﻿/* CST-247 Enterprise Application Programming III
 * Benchmark
 * IMyBibleRepository, Version 1
 * Gary Davis
 * 09/08/2019
 * This interface contains the required methods.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BibleVerseWebApp.Models;

namespace BibleVerseWebApp.Interfaces
{
    public interface IMyBibleRepository
    {
        BibleVerseDBEntities GetBibleEntry();
        List<BIBLE_VERSES> GetBibleVerses();
    }
}
